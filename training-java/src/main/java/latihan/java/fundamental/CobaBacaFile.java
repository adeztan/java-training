package latihan.java.fundamental;

import id.co.pegadaian.training.java.BacaFileHelper;
import id.co.pegadaian.training.java.Nasabah;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public class CobaBacaFile {

    public static void main(String[] args) {
        BacaFileHelper helper = new BacaFileHelper("src/main/resources/coba.txt");

        try {
            helper.bukaFile();
            List<Nasabah> listNasabah = helper.cariNasabah();
            
            for (Nasabah nasabah : listNasabah) {
                System.err.println("Nasabah dengan ID = "+nasabah.getId()+" atas nama "+nasabah.getNama()+" berjenis kelamin"+nasabah.getJenisKelamin());
            }
            helper.tutupFile();
        } catch (FileNotFoundException ex) {
            System.out.println("File tidak ditemukan");
            ex.printStackTrace();
        } catch (IOException ex) {
            System.out.println("Data tidak sesuai");
            ex.printStackTrace();
        }

    }
}
