package latihan.java.fundamental;

import id.co.pegadaian.training.java.JenisKelamin;
import id.co.pegadaian.training.java.Nasabah;

public class Halo {
    
    private String nama = "Bla";
    
    public Halo(){
    }
    
    public void greeting(){
        System.out.println("");
    }

    public static void main(String[] args) {
        Nasabah nasabah1 = new Nasabah();
        nasabah1.setNama("Bla");
        nasabah1.setJenisKelamin(JenisKelamin.PRIA);
        System.out.println("Nama Nasabah 1 : "+nasabah1.getNama());
        System.out.println("Nasabah ke : "+nasabah1.jumlahNasabah);
        System.out.println("Jenis Kelamin : "+nasabah1.getJenisKelamin());
        
        Nasabah nasabah2 = new Nasabah("Rida",1);
        nasabah2.setJenisKelamin(JenisKelamin.WANITA);
        System.out.println("Nama Nasabah 2 : "+nasabah2.getNama());
        System.out.println("Nasabah ke : "+nasabah2.jumlahNasabah);
        System.out.println("Jenis Kelamin : "+nasabah2.getJenisKelamin());
        
        Nasabah nasabah3 = new Nasabah("Rudi",1);
        System.out.println("Nama Nasabah 3 : "+nasabah3.getNama());
        System.out.println("Nasabah ke : "+nasabah3.jumlahNasabah);
        nasabah3.jumlahNasabah = 20;
        
        System.out.println("Nasabah ke : "+Nasabah.jumlahNasabah);
    }
}
