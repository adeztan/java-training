package latihan.java.fundamental;

import id.co.pegadaian.training.java.JenisKelamin;
import id.co.pegadaian.training.java.Nasabah;
import id.co.pegadaian.training.java.NasabahDao;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CobaAksesDb {

    public static void main(String[] args) {
        NasabahDao nasabahDao = new NasabahDao();

        try {
            nasabahDao.connect();
            Nasabah nas = new Nasabah();
            nas.setId("1");
            nas.setNama("Bule");
            nas.setJenisKelamin(JenisKelamin.PRIA);
            nasabahDao.saveData(nas);
            nasabahDao.disconnect();
        } catch (SQLException ex) {
            Logger.getLogger(CobaAksesDb.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(CobaAksesDb.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
