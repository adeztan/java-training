package id.co.pegadaian.training.java;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class NasabahDao {

    private String dbDriver = "com.mysql.jdbc.Driver";
    private String dbUrl = "jdbc:mysql://127.0.0.1:1111/latihan_java";
    private String dbUser = "root";
    private String dbPass = "123";

    Connection con = null;

    private final String SQL_INSERT = "INSERT INTO nasabah (id, nama, jk) values (?,?,?)";

    public void connect() throws SQLException, ClassNotFoundException {
        Class.forName(dbDriver);
        con = DriverManager.getConnection(dbUrl, dbUser, dbPass);
        System.out.println("Sudah Koneksi database");
    }

    public void disconnect() throws SQLException {
        if (con != null) {
            con.close();
        }
    }

    public void saveData(Nasabah nasabah) throws SQLException {
        PreparedStatement ps = con.prepareStatement(SQL_INSERT);
        
        ps.setString(1, nasabah.getId());
        ps.setString(2, nasabah.getNama());
        ps.setString(3, nasabah.getJenisKelamin().toString());
        
        ps.execute();
    }

}
