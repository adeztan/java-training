package id.co.pegadaian.training.java;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class BacaFileHelper {

    File file;
    BufferedReader bf;

    public BacaFileHelper(String inputFile) {
        file = new File(inputFile);
    }

    public void bukaFile() throws FileNotFoundException {
        if (file.exists()) {
            FileReader fileReader = new FileReader(file);
            bf = new BufferedReader(fileReader);
        }
    }

    public void tutupFile() throws IOException {
        if (bf != null) {
            bf.close();
        }
    }

    public List<Nasabah> cariNasabah() throws IOException {
        List<Nasabah> listNasabah = new ArrayList<>();
        
        String data = bf.readLine();
        data = bf.readLine();

        while (data != null) {
            Nasabah nasabah = new Nasabah();
            String[] dat = data.split(",");
            nasabah.setId(dat[0]);
            nasabah.setNama(dat[1]);
            if (dat[2].equals("PRIA")){
                nasabah.setJenisKelamin(JenisKelamin.PRIA);
            } else {
                nasabah.setJenisKelamin(JenisKelamin.WANITA);
            }
            listNasabah.add(nasabah);
            
            data = bf.readLine();
        }
        
        return listNasabah;
    }

    public List<Nasabah> listNasabah() {
        return null;
    }
;

}
