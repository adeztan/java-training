package id.co.pegadaian.training.java;

public class Nasabah extends BaseEntity{
    private String nama;
    private JenisKelamin jenisKelamin;
    public static int jumlahNasabah = 0;
    
    public Nasabah(){
    }
    
    public Nasabah(String nama, int jml){
        this.nama = nama;
        jumlahNasabah = jumlahNasabah+1;
    }
    
    public void setNama (String nama){
        this.nama = nama;
    }
    
    public String getNama (){
        return this.nama;
    }
    
    public JenisKelamin getJenisKelamin() {
        return jenisKelamin;
    }

    public void setJenisKelamin(JenisKelamin jenisKelamin) {
        this.jenisKelamin = jenisKelamin;
    }
}
