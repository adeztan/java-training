package id.co.pegadaian.training.java;

public enum JenisKelamin {
    PRIA, WANITA;
}
