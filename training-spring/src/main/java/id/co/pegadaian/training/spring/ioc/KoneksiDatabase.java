package id.co.pegadaian.training.spring.ioc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class KoneksiDatabase {

    private String dbDriver ;
    private String dbUrl;
    private String dbUser ;
    private String dbPass;

    Connection con = null;

    public void connect() throws SQLException, ClassNotFoundException {
        Class.forName(dbDriver);
        con = DriverManager.getConnection(dbUrl, dbUser, dbPass);
        System.out.println("Sudah Koneksi database");
    }

    public void disconnect() throws SQLException {
        System.out.println("Disconnect");
        if (con != null) {
            con.close();
        }
    }

    public String getDbDriver() {
        return dbDriver;
    }

    public void setDbDriver(String dbDriver) {
        this.dbDriver = dbDriver;
    }

    public String getDbUrl() {
        return dbUrl;
    }

    public void setDbUrl(String dbUrl) {
        this.dbUrl = dbUrl;
    }

    public String getDbUser() {
        return dbUser;
    }

    public void setDbUser(String dbUser) {
        this.dbUser = dbUser;
    }

    public String getDbPass() {
        return dbPass;
    }

    public void setDbPass(String dbPass) {
        this.dbPass = dbPass;
    }

}
