package id.co.pegadaian.training.spring.ioc;

import java.sql.SQLException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MainIocSpring {

    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-ioc.xml");
        
        KoneksiDatabase koneksiDatabase = context.getBean("koneksiDatabaseBean", KoneksiDatabase.class);
        NasabahDao nasabahDao = context.getBean("nasabahDaoBean", NasabahDao.class);
        nasabahDao.testKoneksi();
    }

}
