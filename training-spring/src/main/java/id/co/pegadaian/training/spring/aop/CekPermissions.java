package id.co.pegadaian.training.spring.aop;

import java.util.Date;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.springframework.stereotype.Component;

@Component
public class CekPermissions implements MethodInterceptor{

    @Override
    public Object invoke(MethodInvocation targetObject) throws Throwable {
        System.out.println("Menjalankan advice Cek Permission before di method : "+targetObject.getMethod().getName()+" pada tanggal "+new Date());
        Object result = targetObject.proceed();
        return result;
    }
    
}
