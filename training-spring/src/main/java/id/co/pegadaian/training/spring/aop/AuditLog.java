package id.co.pegadaian.training.spring.aop;

import java.util.Date;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.springframework.stereotype.Component;

@Component
public class AuditLog implements MethodInterceptor{

    @Override
    public Object invoke(MethodInvocation targetObject) throws Throwable {
        Object result = targetObject.proceed();
        System.out.println("Menjalankan advice Audit Log After di method : "+targetObject.getMethod().getName()+" pada tanggal "+new Date());
        return result;
    }
    
}
