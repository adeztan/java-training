package id.co.pegadaian.training.spring.ioc;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class NasabahDao {

    private KoneksiDatabase conDatabase;

    private final String SQL_INSERT = "INSERT INTO nasabah (id, nama, jk) values (?,?,?)";

    public NasabahDao() {
    }
    
    public NasabahDao(KoneksiDatabase database) {
        this.conDatabase = database;
    }

    public void saveData(Nasabah nasabah) throws SQLException, ClassNotFoundException {
        conDatabase.connect();

        PreparedStatement ps = conDatabase.con.prepareStatement(SQL_INSERT);

        ps.setString(1, nasabah.getId());
        ps.setString(2, nasabah.getNama());
        ps.setString(3, nasabah.getJenisKelamin().toString());

        ps.executeUpdate();
        
        conDatabase.disconnect();
        
        System.out.println("Data Berhasil di simpan");
    }
    
    public void testKoneksi() throws SQLException, ClassNotFoundException{
        conDatabase.connect();
    }

    public KoneksiDatabase getConDatabase() {
        return conDatabase;
    }

    public void setConDatabase(KoneksiDatabase conDatabase) {
        this.conDatabase = conDatabase;
    }

}
