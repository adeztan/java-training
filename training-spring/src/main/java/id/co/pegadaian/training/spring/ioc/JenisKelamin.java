package id.co.pegadaian.training.spring.ioc;

public enum JenisKelamin {
    PRIA, WANITA;
}
