package id.co.pegadaian.training.spring.ioc;

import java.sql.SQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class RekeningDao {
    
    @Autowired
    @Qualifier("koneksiDatabaseBean") //apabila namanya berbeda
    private KoneksiDatabase koneksiDatabase;
    
    public void simpanRekening(String norek) throws SQLException, ClassNotFoundException {
        koneksiDatabase.connect();
    }
    
}
