package id.co.pegadaian.training.spring.ioc;

import java.sql.SQLException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MainRekeningSpring {

    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-ioc.xml");

        RekeningDao rekeningDao = context.getBean("rekeningDao", RekeningDao.class);
        rekeningDao.simpanRekening("123");
    }

}
