package id.co.pegadaian.training.spring.aop;

import id.co.pegadaian.training.spring.ioc.KoneksiDatabase;
import java.sql.SQLException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class CobaAop {
    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-ioc.xml");
        
        KoneksiDatabase koneksiDatabase = context.getBean("koneksiDatabaseAop", KoneksiDatabase.class);
        koneksiDatabase.disconnect();
           koneksiDatabase.connect();
        
    }
}
