package id.co.pegadaian.training.spring.ioc;

import java.sql.SQLException;

public class MainIoc {
    
     public static void main(String[] args) throws SQLException, ClassNotFoundException {
        KoneksiDatabase koneksi = new KoneksiDatabase();
        
        koneksi.setDbDriver("com.mysql.jdbc.Driver");
        koneksi.setDbUrl("jdbc:mysql://127.0.0.1:1111/latihan_java");
        koneksi.setDbUser("root");
        koneksi.setDbPass("123");
        
        NasabahDao nasabahDao = new NasabahDao(koneksi);
        
        Nasabah nasabah = new Nasabah();
        nasabah.setId("3");
        nasabah.setNama("fdgdg");
        nasabah.setJenisKelamin(JenisKelamin.WANITA);
        nasabahDao.saveData(nasabah);
    }
    
}
