package id.co.pegadaian.training.spring.jdbc;

import java.sql.SQLException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class CobaSpringJdbc {
     public static void main(String[] args) throws SQLException, ClassNotFoundException {
        
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-jdbc.xml");
        
        NasabahDao nasabahDao = context.getBean("nasabahDao",NasabahDao.class);
        
        Nasabah nasabah = new Nasabah();
        nasabah.setId("00124");
        nasabah.setNama("Rudias");
        nasabah.setJenisKelamin(JenisKelamin.PRIA);
        
        nasabahDao.simpan(nasabah);
    }
}
