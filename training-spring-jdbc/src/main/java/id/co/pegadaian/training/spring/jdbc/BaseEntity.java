package id.co.pegadaian.training.spring.jdbc;

public class BaseEntity {
   private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
}
