package id.co.pegadaian.training.spring.jdbc;

public enum JenisKelamin {
    PRIA, WANITA;
}
