package id.co.pegadaian.training.spring.jdbc;

import java.sql.SQLException;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

@Service
public class NasabahDao {

    @Autowired
    private DataSource dataSource;

    private final String SQL_INSERT = "insert into nasabah (id,nama,jk) values (?,?,?)";

    public void simpan(Nasabah nasabah) throws SQLException, ClassNotFoundException {

        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        jdbcTemplate.update("insert into nasabah (id,nama,jk) values (?,?,?)",
                nasabah.getId(),
                nasabah.getNama(),
                nasabah.getJenisKelamin().toString());

    }
}
