
create table nasabah (id varchar(36) not null, alamat varchar(255), nama varchar(255), no_ktp varchar(255), primary key (id)) ;
create table user_group (id varchar(36) not null, description varchar(255), name varchar(255), primary key (id)) ;
create table group_permission (id_group varchar(36) not null, id_permission varchar(36) not null, primary key (id_group, id_permission)) ;
create table permission (id varchar(36) not null, permission_label varchar(255), permission_value varchar(255), primary key (id)) ;
create table user (id_group varchar(36) not null, fullname varchar(255), username varchar(255), primary key (id_group)) ;
create table user_password (id_user varchar(36) not null, password varchar(255), primary key (id_user)) ;

alter table nasabah add constraint UK_cx9g94iqwam8vm3xegun25mj8 unique (no_ktp);
alter table group_permission add constraint FKl5vuy123sq9coq8mfmbb4oquy foreign key (id_permission) references permission (id);
alter table group_permission add constraint FK181lfdepioo2viggkidpcgngc foreign key (id_group) references user_group (id);
alter table user add constraint FKi8887346yrxluq2oogvtncjg5 foreign key (id_group) references user_group (id);
alter table user_password add constraint FKc6ej0m55bov3brbfiih66sj5i foreign key (id_user) references user (id_group);