package id.co.pegadaian.trainingspringboot.entity;

import javax.persistence.*;

@Table
@Entity
public class UserPassword extends BaseEntity {
    @MapsId
    @OneToOne
    @JoinColumn(name = "id_user", nullable = false, columnDefinition = "varchar(36)")
    private User user;

    private String password;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
