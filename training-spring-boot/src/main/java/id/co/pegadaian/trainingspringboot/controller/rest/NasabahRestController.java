package id.co.pegadaian.trainingspringboot.controller.rest;

import id.co.pegadaian.trainingspringboot.dao.NasabahDao;
import id.co.pegadaian.trainingspringboot.entity.Nasabah;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class NasabahRestController {

    @Autowired
    private NasabahDao nasabahDao;

    @RequestMapping(method = RequestMethod.POST, value = "/nasabah/save")
    public void save(@RequestBody Nasabah nasabah) {
        nasabahDao.save(nasabah);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/nasabah/all")
    public List<Nasabah> searchAll() {
        List<Nasabah> list = (List<Nasabah>) nasabahDao.findAll();

        return list;
    }

    @GetMapping("/nasabah/cariSemuaNasabahPage")
    public Page<Nasabah> searchAllWithPage(@PageableDefault(size = 5) Pageable pageable) {
        Page<Nasabah> ns = nasabahDao.findAll(pageable);

        return ns;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/nasabah/search/{id}")
    public Nasabah searchById(@PathVariable String id) {
        Optional<Nasabah> nasabah = nasabahDao.findById(id);

        if (nasabah.isPresent()){
            return nasabah.get();
        } else {
            return null;
        }
    }

    @RequestMapping(method = RequestMethod.POST, value = "/nasabah/search/searchnama")
    public List<Nasabah> searchByName(@RequestParam(name = "nama") String nama) {
        List<Nasabah> list = nasabahDao.findByNama(nama);

        return list;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/nasabah/search/searchidnama")
    public List<Nasabah> searchByIdAndNama(@RequestParam String id, String nama) {
        List<Nasabah> list = nasabahDao.findByIdAndNama(id, nama);

        return list;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/nasabah/delete/{id}")
    public void deleteById(@PathVariable String id) {
        Optional<Nasabah> nasabah = nasabahDao.findById(id);

        if (nasabah.isPresent()){
            nasabahDao.delete(nasabah.get());
        }
    }
}
