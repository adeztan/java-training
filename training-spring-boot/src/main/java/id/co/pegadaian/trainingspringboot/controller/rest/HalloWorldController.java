package id.co.pegadaian.trainingspringboot.controller.rest;

import id.co.pegadaian.trainingspringboot.entity.Nasabah;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HalloWorldController {

    @RequestMapping(method = RequestMethod.GET, value = "/tes")
    public String hallo(){
        return "Test";
    };

    @GetMapping("/getNasabah")
    public Nasabah getNasabah(){

        Nasabah nasabah = new Nasabah();
        nasabah.setId("1");
        nasabah.setNama("dwaft");
        nasabah.setAlamat("jauh");

        return nasabah;
    }

}
