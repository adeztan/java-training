package id.co.pegadaian.trainingspringboot.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

@Table
@Entity(name = "nasabah")
public class Nasabah extends BaseEntity {

    @NotEmpty(message = "Nama Tidak Boleh Kosong")
    private String nama;

    @NotEmpty(message = "Nama Tidak Boleh Kosong")
    @Column(unique = true)
    private String noKtp;


    @NotEmpty(message = "Nama Tidak Boleh Kosong")
    private String alamat;

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getNoKtp() {
        return noKtp;
    }

    public void setNoKtp(String noKtp) {
        this.noKtp = noKtp;
    }
}
