package id.co.pegadaian.trainingspringboot.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table
public class Permission extends BaseEntity {

    private String permissionLabel;
    private String permissionValue;

    public String getPermissionLabel() {
        return permissionLabel;
    }

    public void setPermissionLabel(String permissionLabel) {
        this.permissionLabel = permissionLabel;
    }

    public String getPermissionValue() {
        return permissionValue;
    }

    public void setPermissionValue(String permissionValue) {
        this.permissionValue = permissionValue;
    }
}
