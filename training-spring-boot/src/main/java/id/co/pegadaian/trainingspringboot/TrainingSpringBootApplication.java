package id.co.pegadaian.trainingspringboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.thymeleaf.dialect.springdata.SpringDataDialect;

@SpringBootApplication
public class TrainingSpringBootApplication {

    public static void main(String[] args) {
        SpringApplication.run(TrainingSpringBootApplication.class, args);
    }

    @Bean
    public SpringDataDialect springDataDialect() {
		return new SpringDataDialect();
    }
}
