package id.co.pegadaian.trainingspringboot.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HalloController {

    @GetMapping("/halo/template")
    public String halo(){
        return "halo";
    }

    @GetMapping("/")
    public String layout () {
        return "layout";
    }

}
