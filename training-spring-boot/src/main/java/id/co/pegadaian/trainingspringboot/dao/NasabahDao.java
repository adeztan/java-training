package id.co.pegadaian.trainingspringboot.dao;

import id.co.pegadaian.trainingspringboot.entity.Nasabah;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface NasabahDao extends PagingAndSortingRepository<Nasabah, String> {

    public List<Nasabah> findByNama(String nama);

    public List<Nasabah> findByIdAndNama(String id, String nama);

    @Query("select u from nasabah u where u.id = :id or u.nama = :nama")
    Nasabah findByLastnameOrFirstname(@Param("id") String lastname,
                                   @Param("nama") String firstname);

}
