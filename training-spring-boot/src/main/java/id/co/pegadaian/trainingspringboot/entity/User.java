package id.co.pegadaian.trainingspringboot.entity;

import javax.persistence.*;

@Table
@Entity
public class User extends BaseEntity {
    private String username;
    private String fullname;

    @OneToOne
    @MapsId
    @JoinColumn(name = "id_group", nullable = false, columnDefinition = "varchar(36)")
    private UserGroup group;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public UserGroup getGroup() {
        return group;
    }

    public void setGroup(UserGroup group) {
        this.group = group;
    }
}
